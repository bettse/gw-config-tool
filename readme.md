from https://wiki.aprbrother.com/en/Software_AB_BLE_Gateway_V4.html
### How To Run ###


* Install yarn 
* Run command

```
yarn start
```

### How To Package ###

* Install electron-builder
```
yarn global add electron-builder
```
* 打包windows版本
```
electron-builder -w
```
* 在dist目录下有安装包生成
